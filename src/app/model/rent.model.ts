import { Pupil } from "./pupil.model";
import { Device } from "./device.model";

export class Rent{
    id: number;
    borrowDate: Date;
    returnDate: Date;
    devices: Device[];
    pupil: Pupil;
}