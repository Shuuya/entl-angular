import { Rent } from "./rent.model";

export class Pupil{
    id: number;
    firstName: string;
    lastName: string;
    schoolClass: string;
    email: string;
    rentList: Rent[];
}