import { Rent } from "./rent.model";

export class Device{
    id: number;
    barcodeNr: number;
    deviceName: string;
    deviceDescription: string;
    visible: boolean;
    device: Device;
    deviceList: Device[];
    rent: Rent;
}