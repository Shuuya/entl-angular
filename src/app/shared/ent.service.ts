import { Injectable } from '@angular/core';
import { Pupil } from '../model/pupil.model';
import { Device } from '../model/device.model';

@Injectable()
export class EntService {
    classList: string[] = [];
    classListI_M: string[] = [];
    classListM_E: string[] = [];
    classListL3: string[] = [];

    selectedClass: string;

    pupilList: Pupil[] = [];
    pupilListSelectedClass: Pupil[] = [];

    selectedPupil: Pupil;

    visDevices: Device[] = [];
    invisDevices: Device[] = [];

    selectedDeviceToGiveBack: Device;
    selectedDevices: Device[] = [];

    deviceToAdd: Device = new Device;

    selectedFromDate: Date;
    selectedToDate: Date;

    lastBarcodeNr: number;
}