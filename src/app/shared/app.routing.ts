import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { UntisComponent } from '../components/untis/untis.component';
import { LoginComponent } from '../components/login/login.component';
import { PupilsComponent } from '../components/pupils/pupils.component';
import { DevicesComponent } from '../components/devices/devices.component';
import { RentComponent } from '../components/rent/rent.component';
import { InvisDevicesComponent } from '../components/invis-devices/invis-devices.component';
import { AddDeviceComponent } from '../components/add-device/add-device.component';
import { BarcodeComponent } from '../components/barcode/barcode.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent},
    { path: 'untis', component: UntisComponent},
    { path: 'login', component: LoginComponent},
    { path: 'pupils', component: PupilsComponent},
    { path: 'devices', component: DevicesComponent},
    { path: 'rent', component: RentComponent},
    { path: 'invisDevices', component: InvisDevicesComponent},
    { path: 'addDevice', component: AddDeviceComponent},
    { path: 'barcode', component: BarcodeComponent},
    { path: '', redirectTo: '/home', pathMatch: 'full'}
];

export const routing = RouterModule.forRoot(appRoutes);