import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Pupil } from '../model/pupil.model';
import { Device } from '../model/device.model';
import { EntService } from './ent.service';

@Injectable()
export class DataService{
    API_ENTL_URL: string = "http://localhost:8081/ausleihsystem/rs/";
    API_ENTL_URL_Pupil: string = "pupil";
    API_ENTL_URL_Device: string = "device";
    API_ENTL_URL_Rent: string = "rent";
    API_ENTL_URL_Device_Visible: string = "/visD";
    API_ENTL_URL_Device_Invisible: string = "/invisD";
    API_ENTL_URL_Rent_RentDevice: string = "/rentDevice";
    API_ENTL_URL_Login: string = "login";
    API_ENTL_URL_Login_CheckUser: string = "/checkuser";
    API_ENTL_URL_Barcode_LastBarcode: string = "/getlastdevicenr";


    API_UNTIS_URL: string = "https://mese.webuntis.com/WebUntis/jsonrpc.do"; 
    API_UNTIS_URL_School: string = "?school=htbla linz leonding";      
    API_UNTIS_URL_SeassionId: string = ";jsessionid=";      
    
    constructor(private http: Http, private entService: EntService){        
    }

    authenticateUntisUser(user: string, password: string){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify({"id": "ID", "method": "authenticate", "params": {
            "user": user, "password": password, "client": "CLIENT"},"jsonrpc": "2.0"});
        let params = json;

        return this.http.post(this.API_UNTIS_URL + this.API_UNTIS_URL_School, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getClassList(seassionId: string){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify({"id": "ID", "method": "getKlassen", "jsonrpc": "2.0"});
        let params = json;

        return this.http.post(this.API_UNTIS_URL + this.API_UNTIS_URL_SeassionId + seassionId, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getPupil(){
        let headers = new Headers({'Content-Type': 'applocation/json'});

        return this.http.get(this.API_ENTL_URL + this.API_ENTL_URL_Pupil)
        .map((res:Response) => res.json() as Pupil[])
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getVisibleDevices(){
        let headers = new Headers({'Content-Type': 'applocation/json'});

        return this.http.get(this.API_ENTL_URL + this.API_ENTL_URL_Device + this.API_ENTL_URL_Device_Visible)
        .map((res:Response) => res.json() as Device[])
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getInvisibleDevices(){
        let headers = new Headers({'Content-Type': 'applocation/json'});

        return this.http.get(this.API_ENTL_URL + this.API_ENTL_URL_Device + this.API_ENTL_URL_Device_Invisible)
        .map((res:Response) => res.json() as Device[])
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    postRent(){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify({"borrowDate": this.entService.selectedFromDate, "returnDate": this.entService.selectedToDate,
         "pupil": this.entService.selectedPupil, "devices": this.entService.selectedDevices });
        let params = json;

        console.log(json)
        return this.http.post(this.API_ENTL_URL + this.API_ENTL_URL_Rent + this.API_ENTL_URL_Rent_RentDevice, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    loginUser(username: string, hashV: string){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify({userName: username, userPassword: hashV});
        let params = json;

        return this.http.post(this.API_ENTL_URL + this.API_ENTL_URL_Login + this.API_ENTL_URL_Login_CheckUser, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    returnDevice(){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify(this.entService.selectedDeviceToGiveBack);
        let params = json;

        return this.http.put(this.API_ENTL_URL + this.API_ENTL_URL_Device, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    addDevice(){
        let headers = new Headers({'Content-Type': 'application/json', });
        let json = JSON.stringify(this.entService.deviceToAdd);
        let params = json;

        return this.http.put(this.API_ENTL_URL + this.API_ENTL_URL_Device, params, {headers: headers})
        .map((res:Response) => res.json())
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getLastBarcodeNr(){
        let headers = new Headers({'Content-Type': 'applocation/json'});
        
        return this.http.get(this.API_ENTL_URL + this.API_ENTL_URL_Device + this.API_ENTL_URL_Barcode_LastBarcode)
        .map((res:Response) => res.json() as number)
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}