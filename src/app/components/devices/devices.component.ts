import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';
import { Device } from '../../model/device.model';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
    this.dataService.getVisibleDevices().subscribe(
      data => {
        this.entService.visDevices = [];
        this.entService.visDevices = data;
    });
  }

  confirmRent(selectedDevice: Device){
    this.entService.selectedDevices.push(selectedDevice);
    this.router.navigate(['/rent']);
  }
}
