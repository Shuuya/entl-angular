import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.component.html',
  styleUrls: ['./barcode.component.css']
})
export class BarcodeComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
    this.dataService.getLastBarcodeNr().subscribe(
      data => {
        this.entService.lastBarcodeNr = data;
      }
    );
  }

}
