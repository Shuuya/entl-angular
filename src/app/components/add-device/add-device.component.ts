import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
  }

  addDevice(barcodeNr: number, deviceName: string, deviceDescription: string){
    this.entService.deviceToAdd.barcodeNr = barcodeNr;
    this.entService.deviceToAdd.device = null;
    this.entService.deviceToAdd.deviceDescription = deviceDescription;
    this.entService.deviceToAdd.deviceName = deviceName;
    this.entService.deviceToAdd.rent = null;
    this.entService.deviceToAdd.visible = true;

    this.dataService.addDevice().subscribe(
      data => {
        alert("Device succesfully added!")
        this.router.navigate(['/invisDevices']);
      }
    );
  }
}
