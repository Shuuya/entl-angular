import { Component, OnInit } from '@angular/core';
import * as sha512 from 'sha512';
import { DataService } from '../../shared/data.service';
import { EntService } from '../../shared/ent.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
  }

  verifyUser(username, password){
    var sha512 = require('sha512')
    var hash = sha512(password);
    password = hash.toString('hex');

    this.dataService.loginUser(username, password).subscribe(
      data => {
        this.router.navigate(['/invisDevices']);
        this.dataService.getInvisibleDevices().subscribe(
          data => {
            this.entService.invisDevices = data;
          }
        )
      }
    );
   
  }
}
