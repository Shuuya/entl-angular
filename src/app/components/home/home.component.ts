import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private entService: EntService) { }

  ngOnInit() {
  }

  showPupils(selectedClass: string){
    this.entService.selectedClass = selectedClass;
    
    if(this.entService.classListL3.length < 3){
      this.entService.classListL3.splice(0,0,selectedClass)
    }else{
      this.entService.classListL3.splice(2,1)
      this.entService.classListL3.splice(0,0,selectedClass)
    }

    this.router.navigate(['/pupils']);
  }
}
