import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';

@Component({
  selector: 'app-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.css']
})
export class RentComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
  }

  rentDevice(dateFrom: Date, dateTo: Date){
    this.entService.selectedFromDate = dateFrom;
    this.entService.selectedToDate = dateTo;

    console.log(this.entService.selectedDevices)
    console.log(this.entService.selectedPupil)

    this.dataService.postRent().subscribe(
      data => {
        alert("Succesfully borrowed device!")
      }
    );
    
    this.router.navigate(['/home']);
  }
}
