import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { EntService } from '../../shared/ent.service';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { Pupil } from '../../model/pupil.model';

@Component({
  selector: 'app-pupils',
  templateUrl: './pupils.component.html',
  styleUrls: ['./pupils.component.css']
})
export class PupilsComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
     private entService: EntService) { }

  ngOnInit() {
    this.dataService.getPupil().subscribe(
      data => {
        this.entService.pupilList = [];
        this.entService.pupilListSelectedClass = [];

        this.entService.pupilList = data;
        this.entService.pupilList.forEach(element => {
          if(element.schoolClass.match(this.entService.selectedClass)){
            this.entService.pupilListSelectedClass.push(element);
          }
        });
      }
    )
  }

  showDevices(selectedPupil: Pupil){
    this.entService.selectedPupil = selectedPupil;
    this.router.navigate(['/devices']);
  }
}
