import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvisDevicesComponent } from './invis-devices.component';

describe('InvisDevicesComponent', () => {
  let component: InvisDevicesComponent;
  let fixture: ComponentFixture<InvisDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvisDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvisDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
