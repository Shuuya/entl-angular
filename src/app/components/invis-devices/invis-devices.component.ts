import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/data.service';
import { Router } from '@angular/router';
import { EntService } from '../../shared/ent.service';
import { Device } from '../../model/device.model';

@Component({
  selector: 'app-invis-devices',
  templateUrl: './invis-devices.component.html',
  styleUrls: ['./invis-devices.component.css']
})
export class InvisDevicesComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { }

  ngOnInit() {
  }

  giveBack(selectedDeviceToGiveBack: Device){
    this.entService.selectedDeviceToGiveBack = selectedDeviceToGiveBack;

    this.entService.selectedDeviceToGiveBack.visible = true;
    this.dataService.returnDevice().subscribe(
      data => {
        this.dataService.getInvisibleDevices().subscribe(
          data => {
            this.entService.invisDevices = [];
            this.entService.invisDevices = data;
          }
        );
        alert("Device succesfully taken back!");
      }
    );
  }
}
