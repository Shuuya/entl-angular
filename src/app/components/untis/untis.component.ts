import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../shared/data.service';
import { EntService } from '../../shared/ent.service';

@Component({
  selector: 'app-untis',
  templateUrl: './untis.component.html',
  styleUrls: ['./untis.component.css']
})
export class UntisComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router,
    private entService: EntService) { 

  }

  ngOnInit() {
  }

  updateClassList(username: string, password: string){
    this.dataService.authenticateUntisUser(username, password).subscribe(
      data => {
        this.dataService.getClassList(data['result']['sessionId']).subscribe(
          data => {
            this.entService.classList = [];
            this.entService.classListI_M = [];
            this.entService.classListM_E = [];

            data.result.forEach(element => {
              if ((element.name).match("^1")
                || (element.name).match("^2")
                || (element.name).match("^3")
                || (element.name).match("^4")
                || (element.name).match("^5")) {
                  this.entService.classList.push(element.name);
                }
            });

            this.entService.classList.forEach(element => {
              if (element.match("HIF$") || element.match("HITM$")) {
                this.entService.classListI_M.push(element);
              }else if(element.match("BG$") || element.match("HEL$")) {
                this.entService.classListM_E.push(element);
              }
            });
          }
        );
        alert("Class list updated!")
        this.router.navigate(['/home']);
      }
    );
    alert("Couldn't update class list!")
  }
}
