import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { routing } from './shared/app.routing';
import { DataService } from './shared/data.service';
import { HomeComponent } from './components/home/home.component';
import { UntisComponent } from './components/untis/untis.component';
import { EntService } from './shared/ent.service';
import { LoginComponent } from './components/login/login.component';
import { PupilsComponent } from './components/pupils/pupils.component';
import { DevicesComponent } from './components/devices/devices.component';
import { RentComponent } from './components/rent/rent.component';

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/observable/of";
import { InvisDevicesComponent } from './components/invis-devices/invis-devices.component';
import { AddDeviceComponent } from './components/add-device/add-device.component';
import { BarcodeComponent } from './components/barcode/barcode.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UntisComponent,
    LoginComponent,
    PupilsComponent,
    DevicesComponent,
    RentComponent,
    InvisDevicesComponent,
    AddDeviceComponent,
    BarcodeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule,
    JsonpModule,
  ],
  providers: [DataService, EntService],
  bootstrap: [AppComponent]
})
export class AppModule { }
